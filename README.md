# RESTons Connecter_Mersad



J'ai commencer par installer Java dans mon pc, 
ensuite j'ai initialiser spring boot , dans  https://start.spring.io/, avec les dependecies , comme mysql , validator, data jpa . Ensuit j'ai connecter avec mysql dans application.properties.

J'ai creer le table Country dans entity/Country.java pour les properties du table.

Le CountryRepository dans repository/CountryRepository.java pour le CRUD.
Le MainController dans controller/MainController.java.
Dans / on trouve index avec le formulaire pour ajouter, modifier un voiture et regarder tout le pays.

J'ai run springboot avec la commande ./mvnw spring-boot:run

Les commandes Curls 


GET /countries => La liste de tous les pays. curl localhost:8080/countries
GET /country/{id} => Les informations du pays portant cet id curl localhost:8080/country/1
POST /country => Enregistre un pays. curl localhost:8080/country -d name=ALbanie -d code=AL 
curl -i -X POST  localhost:8080/country -d name=ALbanie -d code=AL 
PUT /country/{id} => Met à jour un pays portant cet id 
curl -X PUT http://localhost:8080/country/1 -H 'cache-control: no-cache' -H 'Content-type:application/json'  -d '{ "name":"rfdfd", "code":"RE"}'
DELETE /country/{id} => Supprime le pays portant cet id curl -i -X DELETE localhost:8080/country/1 
![image.png](./image.png)
![image-1.png](./image-1.png)
![image-2.png](./image-2.png)

![image-3.png](./image-3.png)
![image-4.png](./image-4.png)
![image-5.png](./image-5.png)
![image-6.png](./image-6.png)
![image-7.png](./image-7.png)
​
