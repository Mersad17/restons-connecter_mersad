let displayCountries = document.querySelector('.displayCountries');

   fetch('http://localhost:8080/countries',{
        method: 'GET',
        mode: 'no-cors',
        headers: {
          'Content-Type': 'application/json',
        },
      }).then(response => response.json())
      .then(renderCountries);     

function renderCountries(countries) {
    countries.forEach(function (countries,i) {
        country_div = document.createElement('div');
        country_div.id = 'country-div' + i;
        country_div.class = 'country-card';

country_div.innerHTML = `<div class="card border-0 shadow"> 
    <div class="card-body text-center"> 
    <h5 class=" card-title mb-0 country-name" id="name-country">${countries.name}</h5>
    <input type="text" class="text-center displayNone country-up-name" id="updateName" value=" ${countries.name}">
    <input type="text" class="text-center displayNone country-up-code" id="updateCode" value="${countries.code}">
    <div class=" card-text text-black-50 country-code" id="code-country">
    ${countries.code}</div>
    <a class=" btn btn-info see text-small m-2  upbtn" onclick="showUpForm(${i})">
    <small> Modifier</small></a><a class="btn btn-success see text-small m-2 validate displayNone" onclick="updateCountry(${i},${countries.id})">
    <small>Valider</small></a><a class="btn btn-danger delete displayNone small m-2 " onClick="deleteCountry(${countries.id});"> 
    <small>Supprimer</small>
    </a></div>`
displayCountries.appendChild(country_div);
  })
}

const el = document.getElementById("submit");
el.addEventListener("click", addCountry);

function addCountry() {
    let name = document.getElementById("name");
    let code = document.getElementById("code");
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    let raw = JSON.stringify({
        "name": name.value,
        "code": code.value
    });
    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };
    fetch("http://localhost:8080/country", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .then( location.reload())
        .catch(error => console.log('error', error));
}

    //Delete
        function deleteCountry(id){
            console.log(id)
        let requestOptions = {
            method: 'DELETE',
            redirect: 'follow'
        };
        fetch(`http://localhost:8080/country/${id}`, requestOptions)
        .then(response => response.json()).then( location.reload())
        }
        //Put
        function updateCountry(i,id){
            console.log(id)
            updateName = document.querySelectorAll('.country-up-name');
            updateCode = document.querySelectorAll('.country-up-code');
            console.log(updateName[i].value)
            console.log(updateCode[i].value)
            var dataObject = {
                name: updateName[i].value,
                code: updateCode[i].value,
            };
        fetch(`http://localhost:8080/country/${id}`,{
            method:'PUT',
            headers:{
            'Content-Type':'application/json'
            },
            body:JSON.stringify(dataObject)
        }).then(response=>{
            return response.json()
        }).then( location.reload()).then(data=> 
        // this is the data we get after putting our data,
        console.log(data)
        );
        }
        function showUpForm(element){
   
            console.log(element);
            fieldName = document.querySelectorAll('.country-name');
            fieldCode = document.querySelectorAll('.country-code');
            fieldName[element].classList.add('displayNone');
            fieldCode[element].classList.add('displayNone');
            
            upName = document.querySelectorAll('.country-up-name');
            upCode = document.querySelectorAll('.country-up-code');
            upCode[element].classList.remove('displayNone');
            upName[element].classList.remove('displayNone');
        
            validateButton = document.querySelectorAll('.validate');
            validateButton[element].classList.remove('displayNone');
        
            updateButton = document.querySelectorAll('.upbtn');
            updateButton[element].classList.add('displayNone');
        
    
        }