package group9.restonsconnecter.repository;

import org.springframework.data.repository.CrudRepository;

import group9.restonsconnecter.entity.Country;

// This will be AUTO IMPLEMENTED by Spring into a Bean called ContryRepository
// CRUD refers Create, Read, Update, Delete

public interface CountryRepository extends CrudRepository<Country, Integer> {

}