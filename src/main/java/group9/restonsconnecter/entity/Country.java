package group9.restonsconnecter.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity // This tells Hibernate to make a table out of this class
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull(message = "Ne doit pas etre vide ")
    @Pattern(regexp = "^[A-Za-z]*$", message = "Doit avoir que des characters")
    private String name;
    @NotNull
    @Size(min = 2, max = 2, message = "Doit contenir que 2 characters")
    @Pattern(regexp = "^[A-Za-z]*$", message = "Doit avoir que des characters")
    private String code;

    public Country(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public Country() {

    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code.toUpperCase();
    }

    public String getCode() {
        return code;
    }
}